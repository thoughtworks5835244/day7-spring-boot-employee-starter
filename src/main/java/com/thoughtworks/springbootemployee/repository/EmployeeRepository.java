package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository
         extends JpaRepository<Employee, Long> {

    List<Employee> findAllByGender(String gender);

    List<Employee> findAllByCompanyId(Long companyId);

    List<Employee> findAllByNameContaining(String name);

    List<Employee> findAllByAgeLessThan(Integer age);

    List<Employee> findAllByAgeBetween(Integer startAge, Integer endAge);

}