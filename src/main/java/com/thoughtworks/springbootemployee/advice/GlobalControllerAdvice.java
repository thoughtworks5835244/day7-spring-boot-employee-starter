package com.thoughtworks.springbootemployee.advice;

import com.thoughtworks.springbootemployee.exception.InvalidEmployeeException;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String resourceNotFoundException(NotFoundException ex, WebRequest request) {
        return ex.getMessage();
    }

    @ExceptionHandler(InvalidEmployeeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String invalidEmployee(InvalidEmployeeException ex, WebRequest request) {
        return ex.getMessage();
    }
}
