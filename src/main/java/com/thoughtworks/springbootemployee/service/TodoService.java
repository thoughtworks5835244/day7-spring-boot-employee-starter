package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Todo;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.repository.TodoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    TodoRepository repo;

    public TodoService(TodoRepository repo) {
        this.repo = repo;
    }

    public List<Todo> findAll() {
        return repo.findAll();
    }

    public Todo findById(Long id) {
        return repo
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Todo not found"));
    }

    public Todo save(Todo todo) {
        if (todo == null) return null;
        return repo.save(todo);
    }

    public Todo update(Long updateTodoId, Todo updatedTodo) {
        Todo todo = findById(updateTodoId);
        BeanUtils.copyProperties(updatedTodo, todo, "id");
        return repo.save(todo);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
