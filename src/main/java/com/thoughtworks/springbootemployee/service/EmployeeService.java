package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.exception.InvalidEmployeeException;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    EmployeeRepository repo;

    public EmployeeService(EmployeeRepository repo) {
        this.repo = repo;
    }

    public List<Employee> findAll() {
        return repo.findAll();
    }

    public Employee findById(Long id) {
        return repo
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Employee not found"));
    }

    public List<Employee> findAllByGender(String gender) {
        return repo.findAllByGender(gender);
    }

    public List<Employee> findAllByPage(Integer pageNumber, Integer pageSize) {
        return repo
            .findAll(PageRequest.of(pageNumber - 1, pageSize))
            .getContent();
    }

    public Employee save(Employee employee) {
        if (employee == null) return null;

        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new InvalidEmployeeException("Employee must be 18~65 years old");
        }

        employee.setActive(true);
        return repo.save(employee);
    }

    public Employee update(Long updateEmployeeId, Employee newEmployee) {
        Employee employee = findById(updateEmployeeId);

        if (!employee.getActive()) {
            throw new InvalidEmployeeException("Employee is inactive");
        }

        BeanUtils.copyProperties(newEmployee, employee, "id", "active");
        return repo.save(employee);
    }

    public void softDeleteById(Long id) {
        Employee employee = findById(id);
        employee.setActive(false);
        repo.save(employee);
    }

    public List<Employee> getAllEmployeesLessThan(Integer age) {
        return repo.findAllByAgeLessThan(age);
    }

    public List<Employee> getAllEmployeesAgeBetween(Integer start, Integer end) {
        return repo.findAllByAgeBetween(start, end);
    }

    public List<Employee> getAllEmployeesNameContains(String nameString) {
        return repo.findAllByNameContaining(nameString);
    }
}
