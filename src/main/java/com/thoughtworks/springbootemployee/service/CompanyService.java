package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    CompanyRepository repo;

    public CompanyService(CompanyRepository repo) {
        this.repo = repo;
    }

    public List<Company> findAll() {
        return repo.findAll();
    }

    public Company findById(Long id) {
        return repo
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Company not found"));
    }

    public List<Employee> findAllEmployeeByCompanyId(Long companyId) {
        return findById(companyId).getEmployees();
    }
}
