package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.component.EmployeeMapper;
import com.thoughtworks.springbootemployee.dto.EmployeeRequest;
import com.thoughtworks.springbootemployee.dto.EmployeeResponse;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    EmployeeService employeeService;
    EmployeeMapper employeeMapper;

    public EmployeeController(
        EmployeeService employeeService,
        EmployeeMapper employeeMapper
    ) {
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<EmployeeResponse> getAllEmployees() {
        return employeeService
            .findAll()
            .stream()
            .map(employeeMapper::toResponse)
            .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {
        return employeeMapper.toResponse(employeeService.findById(id));
    }

    @GetMapping(params = {"gender"})
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployeesByGender(
        @RequestParam String gender
    ) {
        return employeeService.findAllByGender(gender);
    }

    @GetMapping("/age-less-than")
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployeesLessThan(
        @RequestParam(defaultValue = "35") Integer age
    ) {
        return employeeService.getAllEmployeesLessThan(age);
    }

    @GetMapping("/age-between")
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployeesLessThan(
        @RequestParam(defaultValue = "18") Integer start,
        @RequestParam(defaultValue = "88") Integer end
    ) {
        return employeeService.getAllEmployeesAgeBetween(start, end);
    }

    @GetMapping("/name-contain")
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployeesNameContains(
        @RequestParam(defaultValue = "Rock") String nameString
    ) {
        return employeeService.getAllEmployeesNameContains(nameString);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<Employee> getAllEmployeesByPage(
        @RequestParam Integer pageNumber,
        @RequestParam Integer pageSize
    ) {
        return employeeService.findAllByPage(pageNumber, pageSize);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = employeeMapper.toEntity(employeeRequest);
        Employee savedEmployee = employeeService.save(employee);
        return employeeMapper.toResponse(savedEmployee);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee updateEmployee(
        @PathVariable Long id,
        @RequestBody Employee newEmployee
    ) {
        return employeeService.update(id, newEmployee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id) {
        employeeService.softDeleteById(id);
    }
}
