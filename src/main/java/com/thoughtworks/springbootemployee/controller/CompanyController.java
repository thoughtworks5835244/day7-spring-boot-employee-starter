package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.component.EmployeeMapper;
import com.thoughtworks.springbootemployee.dto.EmployeeResponse;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.service.CompanyService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    CompanyService companyService;
    EmployeeMapper employeeMapper;

    public CompanyController(
        CompanyService companyService,
        EmployeeMapper employeeMapper
    ) {
        this.companyService = companyService;
        this.employeeMapper = employeeMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Company> getAllCompanies() {
        return companyService.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Company getCompanyById(@PathVariable Long id) {
        Company company = companyService.findById(id);
        company.getEmployees().forEach(employee -> employee.setSalary(null));
        return company;
    }

    @GetMapping("/{companyId}/employees")
    @ResponseStatus(HttpStatus.OK)
    public List<EmployeeResponse> getCompanyEmployees(@PathVariable Long companyId) {
        return companyService
            .findAllEmployeeByCompanyId(companyId)
            .stream()
            .map(employeeMapper::toResponse)
            .collect(Collectors.toList());
    }
}
