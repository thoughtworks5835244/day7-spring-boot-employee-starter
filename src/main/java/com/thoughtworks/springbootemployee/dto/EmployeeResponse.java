package com.thoughtworks.springbootemployee.dto;

public class EmployeeResponse {

    private Long id;
    private Long companyId;
    private String name;
    private Integer age;
    private String gender;
    private boolean active;

    public EmployeeResponse(
        Long id,
        String name,
        Integer age,
        String gender,
        boolean active,
        Long companyId
    ) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.active = active;
        this.companyId = companyId;
    }

    public EmployeeResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
