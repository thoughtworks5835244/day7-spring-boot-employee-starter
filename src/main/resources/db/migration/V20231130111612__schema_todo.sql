USE `employeePractice`;

DROP TABLE IF EXISTS `todo`;
CREATE TABLE `todo`
(
    `id`   bigint       NOT NULL AUTO_INCREMENT,
    `text` varchar(255) NOT NULL,
    `done` boolean      NOT NULL,
    PRIMARY KEY (`id`)
)
;
