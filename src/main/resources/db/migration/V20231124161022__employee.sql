USE `employeePractice`;

INSERT INTO `employee` (`name`, `age`, `gender`, `salary`, `active`, `company_id`)
VALUES ('John Smith', 32, 'Male', 5000, true, 1);

INSERT INTO `employee` (`name`, `age`, `gender`, `salary`, `active`, `company_id`)
VALUES ('Jane Johnson', 28, 'Female', 6000, true, 1);

INSERT INTO `employee` (`name`, `age`, `gender`, `salary`, `active`, `company_id`)
VALUES ('David Williams', 35, 'Male', 5500, true, 2);

INSERT INTO `employee` (`name`, `age`, `gender`, `salary`, `active`, `company_id`)
VALUES ('Emily Brown', 23, 'Female', 4500, true, 2);

INSERT INTO `employee` (`name`, `age`, `gender`, `salary`, `active`, `company_id`)
VALUES ('Michael Jones', 40, 'Male', 7500, true, 2);
