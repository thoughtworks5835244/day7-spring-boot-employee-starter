USE `employeePractice`;

DROP TABLE IF EXISTS `company`;
CREATE TABLE `company`
(
    `id`   bigint       NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
)
;

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`
(
    `id`         bigint       NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) NOT NULL,
    `age`        int          NOT NULL,
    `gender`     varchar(255) NOT NULL,
    `salary`     int          NOT NULL,
    `active`     boolean      NOT NULL,
    `company_id` bigint       NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
)
;
