package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    static Company companyOOCL;
    static Company companyCOSCON;

    @BeforeEach
    void setup() {
        employeeRepository.deleteAll();
        companyRepository.deleteAll();

        companyOOCL = companyRepository.save(new Company(null, "OOCL"));
        companyCOSCON = companyRepository.save(new Company(null, "COSCON"));

        employeeRepository.saveAll(List.of(
            new Employee(null, "Jackson", 19, "Male", 2000, true, companyOOCL.getId()),
            new Employee(null, "James", 18, "Male", 2000, true, companyCOSCON.getId())
        ));
    }

    @Test
    void should_return_all_companies_when_get_all_companies() throws Exception {
        mockMvc
            .perform(get("/companies"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)))
        ;
    }

    @Test
    void should_return_company_with_correct_id_when_get_company_by_id() throws Exception {
        mockMvc
            .perform(get("/companies/" + companyOOCL.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(companyOOCL.getId().intValue())))
            .andExpect(jsonPath("$.name", is(companyOOCL.getName())))
            .andExpect(jsonPath("$.employees", hasSize(1)))
            .andExpect(jsonPath("$.employees[0].id", notNullValue()))
            .andExpect(jsonPath("$.employees[0].name", is("Jackson")))
            .andExpect(jsonPath("$.employees[0].age", is(19)))
            .andExpect(jsonPath("$.employees[0].gender", is("Male")))
            .andExpect(jsonPath("$.employees[0].active", is(true)))
            .andExpect(jsonPath("$.employees[0].companyId", is(companyOOCL.getId().intValue())))
            .andExpect(jsonPath("$.employees[0].salary", nullValue()))
        ;
    }

    @Test
    void should_return_employees_from_the_same_company_when_get_employees_by_company_id() throws Exception {
        mockMvc
            .perform(get("/companies/" + companyCOSCON.getId() + "/employees"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$[0].id", notNullValue()))
            .andExpect(jsonPath("$[0].name", is("James")))
            .andExpect(jsonPath("$[0].age", is(18)))
            .andExpect(jsonPath("$[0].gender", is("Male")))
            .andExpect(jsonPath("$[0].active", is(true)))
            .andExpect(jsonPath("$[0].companyId", is(companyCOSCON.getId().intValue())))
            .andExpect(jsonPath("$[0].salary").doesNotExist())
        ;
    }
}
