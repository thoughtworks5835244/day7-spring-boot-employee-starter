package com.thoughtworks.springbootemployee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class EmployeeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    Gson gson = new GsonBuilder().create();

    static Employee jacky;
    static Employee jill;
    static Employee jackson;
    static Employee james;

    static Company companyOOCL;
    static Company companyCOSCON;

    @BeforeEach
    void setup() {
        employeeRepository.deleteAll();
        companyRepository.deleteAll();

        companyOOCL = companyRepository.save(new Company(null, "OOCL"));
        companyCOSCON = companyRepository.save(new Company(null, "COSCON"));

        jacky = employeeRepository.save(new Employee(null, "Jacky", 20, "Female", 2000, true, companyOOCL.getId()));
        jill = employeeRepository.save(new Employee(null, "Jill", 20, "Female", 2000, true, companyOOCL.getId()));
        jackson = employeeRepository.save(new Employee(null, "Jackson", 19, "Male", 2000, true, companyCOSCON.getId()));
        james = employeeRepository.save(new Employee(null, "James", 18, "Male", 2000, true, companyCOSCON.getId()));
    }

    @Test
    void should_return_all_employees_when_get_all_employees() throws Exception {
        mockMvc
            .perform(get("/employees"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(4)))
        ;
    }

    @Test
    void should_return_employee_with_correct_id_when_get_employee_by_id() throws Exception {
        mockMvc
            .perform(get("/employees/" + jill.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(jill.getId().intValue())))
            .andExpect(jsonPath("$.name", is("Jill")))
            .andExpect(jsonPath("$.age", is(20)))
            .andExpect(jsonPath("$.gender", is("Female")))
            .andExpect(jsonPath("$.active", is(true)))
        ;
    }

    @Test
    void should_return_404_not_found_when_get_employee_by_non_existing_id() throws Exception {
        mockMvc
            .perform(get("/employees/999"))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$", is("Employee not found")))
        ;
    }

    @Test
    void should_return_all_employees_with_same_gender_when_get_employee_by_gender() throws Exception {
        mockMvc
            .perform(get("/employees?gender=Female"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)))
            .andExpect(jsonPath("$[0].gender", is("Female")))
            .andExpect(jsonPath("$[1].gender", is("Female")))
        ;
    }

    @Test
    void should_return_paged_employees_when_get_employees_by_page_number_and_size() throws Exception {
        mockMvc
            .perform(get("/employees?pageNumber=1&pageSize=1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(1)))
            .andExpect(jsonPath("$[0].name", is("Jacky")))
        ;

        mockMvc
            .perform(get("/employees?pageNumber=2&pageSize=2"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)))
            .andExpect(jsonPath("$[0].name", is("Jackson")))
            .andExpect(jsonPath("$[1].name", is("James")))
        ;

        mockMvc
            .perform(get("/employees?pageNumber=3&pageSize=3"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(0)))
        ;
    }

    @Test
    void should_save_new_employee_when_post_a_new_employee() throws Exception {
        Employee newEmployee = new Employee(null, "John", 55, "Male", 9999, true, companyOOCL.getId());

        mockMvc
            .perform(
                post("/employees")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(gson.toJson(newEmployee))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name", is("John")))
            .andExpect(jsonPath("$.age", is(55)))
            .andExpect(jsonPath("$.gender", is("Male")))
            .andExpect(jsonPath("$.active", is(true)))
            .andExpect(jsonPath("$.companyId", is(companyOOCL.getId().intValue())))
        ;

        assertEquals(5, employeeRepository.findAll().size());
    }

    @Test
    void should_update_employee_attributes_when_put_a_existing_employee_with_new_field_values() throws Exception {
        jackson.setAge(20);
        jackson.setSalary(2200);

        mockMvc
            .perform(
                put("/employees/" + jackson.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(gson.toJson(jackson))
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(jackson.getId().intValue())))
            .andExpect(jsonPath("$.name", is("Jackson")))
            .andExpect(jsonPath("$.age", is(20)))
            .andExpect(jsonPath("$.gender", is("Male")))
            .andExpect(jsonPath("$.active", is(true)))
        ;
    }

    @Test
    void should_delete_correct_employee_when_delete_employee_by_id() throws Exception {
        mockMvc
            .perform(delete("/employees/" + james.getId()))
            .andExpect(status().isNoContent())
        ;

        mockMvc
            .perform(get("/employees/" + james.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(james.getId().intValue())))
            .andExpect(jsonPath("$.name", is("James")))
            .andExpect(jsonPath("$.age", is(18)))
            .andExpect(jsonPath("$.gender", is("Male")))
            .andExpect(jsonPath("$.active", is(false)))
        ;
    }

    @Test
    void should_return_employees_whose_age_less_than_given_age() throws Exception {
        mockMvc
            .perform(get("/employees/age-less-than"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(4)))
        ;

        mockMvc
            .perform(get("/employees/age-less-than?age=20"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(2)))
        ;

        mockMvc
            .perform(get("/employees/age-less-than?age=19"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(1)))
        ;
    }

    @Test
    void should_return_employees_whose_age_between_given_age_range() throws Exception {
        mockMvc
            .perform(get("/employees/age-between?start=18&end=30"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(4)))
        ;

        mockMvc
            .perform(get("/employees/age-between?start=19&end=30"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(3)))
        ;
    }

    @Test
    void should_return_employees_whose_name_contains_given_string() throws Exception {
        mockMvc
            .perform(get("/employees/name-contain?nameString=Wang"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(0)))
        ;

        mockMvc
            .perform(get("/employees/name-contain?nameString=Ja"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.*", hasSize(3)))
        ;
    }
}
