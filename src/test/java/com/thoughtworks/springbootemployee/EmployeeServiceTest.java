package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.exception.InvalidEmployeeException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.EmployeeService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

    @Test
    void should_return_all_employees_when_get_all_given_all_employees() {
        when(employeeRepository.findAll()).thenReturn(List.of());

        assertEquals(0, employeeService.findAll().size());

        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_throw_exception_when_save_employee_given_an_employee_with_invalid_age() {
        Employee employee = new Employee(0L, "Jane", 15, "Female", 1800, true, 1L);

        InvalidEmployeeException exception = assertThrows(
            InvalidEmployeeException.class,
            () -> employeeService.save(employee)
        );

        assertEquals("Employee must be 18~65 years old", exception.getMessage());
        verify(employeeRepository, times(0)).save(employee);
    }

    @Test
    void should_throw_exception_when_update_employee_given_an_inactive_employee() {
        Employee employee = new Employee(0L, "Jessica", 29, "Female", 2999, false, 1L);
        when(employeeRepository.findById(any())).thenReturn(Optional.of(employee));

        InvalidEmployeeException exception = assertThrows(
            InvalidEmployeeException.class,
            () -> employeeService.update(employee.getId(), employee)
        );

        assertEquals("Employee is inactive", exception.getMessage());
    }
}
